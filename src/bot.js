/**
 * Nurse Joy by Oman Computar
 * License: MIT License
 * https://gitlab.com/OmanComputar/NurseJoy
 */

// Load dotenv before doing anything
require('dotenv').config()

// Import any system modules we may use
import fs           from 'fs'
import path         from 'path'

// Import Discordie and Raven (and set it up!)
import Discordie    from 'discordie'
import Raven        from 'raven'

Raven.config(process.env.SENTRY_DSN).install()

// Set constants for the bot itself
const   bot_token       = process.env.DISCORD_BOT_TOKEN || '',
        client          = new Discordie(),
        Events          = Discordie.Events,
        // Set current directory as main bot's folder
        botPath         = path.dirname(fs.realpathSync(__filename)),
        dataPath        = `${botPath}/../data`,
        // Set command prefixes
        commandPrefixes = ['!', '.'],
        // Set channel and roles constants
        channels        = {},
        roles           = {},
        // Extra variables related to initialization
        ready           = false

// Create data directory if it doesn't exist
if (!fs.existsSync(dataPath)) {
    fs.mkdirSync(dataPath)
}

// Create warns file if it doesn't exist
if (!fs.existsSync(`${dataPath}/warns.json`)) {
    fs.writeFileSync(`${dataPath}/warns.json`, `{}`)
}

// Create silences file if it doesn't exist
if (!fs.existsSync(`${dataPath}/silences.json`)) {
    fs.writeFileSync(`${dataPath}/silences.json`, `{}`)
}

// Create kills file if it doesn't exist
if (!fs.existsSync(`${dataPath}/kills.json`)) {
    fs.writeFileSync(`${dataPath}/kills.json`, `{}`)
}

// Create soft bans file if it doesn't exist
if (!fs.existsSync(`${dataPath}/softbans.json`)) {
    fs.writeFileSync(`${dataPath}/softbans.json`, `{}`)
}

// Create monitored file if it doesn't exist
if (!fs.existsSync(`${dataPath}/monitor.json`)) {
    fs.writeFileSync(`${dataPath}/monitor.json`, `{}`)
}

async function handleKills(server) {
    const kills = require(`${dataPath}/kills.json`),
          serverBans = await server.getBans()
    let killList = {}
    console.log(serverBans)
}

client.Dispatcher.on(Events.GATEWAY_READY, e => {
    client.Guilds.forEach(server => {
        if (server.id != 118264827201191937) {
            console.log(`${client.User.username} has access to a non authorised server! Terminating process!`)
            process.exit(1)
        }
        console.log(`${client.User.username} has started! ${server.name} has ${server.member_count} members!`)

        // Set up references to each channel we need
        const channelCollection = server.textChannels

        channelCollection.forEach(channel => {
            if (channel.id == 283784146868764683) channels.rules = channel.id
            if (channel.id == 296716651041128449) channels.annoucements = channel.id
            if (channel.id == 300714643381288960) channels.events = channel.id
            if (channel.id == 118264827201191937) channels.general = channel.id
            if (channel.id == 336928211445350402) channels.pokeTalk = channel.id
            if (channel.id == 231281489604640768) channels.trades = channel.id
            if (channel.id == 283795832086593537) channels.musicCommands = channel.id
            if (channel.id == 118413309224550406) channels.tournaments = channel.id
            if (channel.id == 336897035036917764) channels.casino = channel.id
            if (channel.id == 330772553692086273) channels.levelCheck = channel.id
            if (channel.id == 231281189472698368) channels.staff = channel.id
            if (channel.id == 324047190001713153) channels.legacyLogs = channel.id
            if (channel.id == 341427069378494466) channels.modLogs = channel.id
            if (channel.id == 341427242028630017) channels.msgLogs = channel.id
            if (channel.id == 341427365831770112) channels.trackingLogs = channel.id
            if (channel.id == 341427465949806593) channels.serverLogs = channel.id
        })

        // Set up references to roles we need
        const roleCollection = server.roles

        roleCollection.forEach(role => {
            if (role.id == 118412789592227844) roles.admin = role.id
            if (role.id == 296176134658326528) roles.musicAdmin = role.id
            if (role.id == 296683743152701440) roles.bot = role.id
            if (role.id == 118412898769960963) roles.mod = role.id
            if (role.id == 293552677940625409) roles.birthday = role.id
            if (role.id == 331491707310833664) roles.shamed = role.id
            if (role.id == 308473257114468352) roles.muted = role.id
            if (role.id == 286520261875073025) roles.champion = role.id
            if (role.id == 330403273934766080) roles.pokeMaster = role.id
            if (role.id == 309408042292871189) roles.eliteFour = role.id
            if (role.id == 283792307898155011) roles.gymLeader = role.id
            if (role.id == 300716755167346688) roles.breeder = role.id
            if (role.id == 283793312442351618) roles.pokeTrainer = role.id
            if (role.id == 310278969062719488) roles.youngster = role.id
            if (role.id == 330928010184228865) roles.collector = role.id
            if (role.id == 118264827201191937) roles.default = role.id
        })

        // Get current bans and unban if required
        handleKills(server)

        /*server.getBans().then(users => {
            users.forEach(user => {
                killList[user.id] = {
                    user: user,
                    expires: new Date(8640000000000000)
                }
            })
            /*let finalList = JSON.stringify(killList)
            fs.writeFileSync(`${dataPath}/kills.json`, finalList)/
        })*/
    })
})

client.connect({token: bot_token})